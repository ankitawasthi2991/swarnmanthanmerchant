package com.suncity.swarnmanthanmerchant.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;
import com.suncity.swarnmanthanmerchant.R;
import com.suncity.swarnmanthanmerchant.helper.APIService;
import com.suncity.swarnmanthanmerchant.helper.RecyclerViewClickListener;
import com.suncity.swarnmanthanmerchant.model.ProductModel;

import java.util.ArrayList;
import java.util.List;

public class AdapterProductListView extends RecyclerView.Adapter<AdapterProductListView.ProductViewHolder> {
    private RecyclerViewClickListener mListener;
    private List<ProductModel> productModelList;
    private Context context;

    public AdapterProductListView(Context context, ArrayList<ProductModel> productModelList, RecyclerViewClickListener listener) {
        this.context = context;
        this.productModelList = productModelList;
        mListener = listener;
    }

    @NonNull
    @Override
    public ProductViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View couponView =
                LayoutInflater.from(parent.getContext()).inflate(R.layout.cell_product, parent, false);
        return new ProductViewHolder(couponView);
    }

    @Override
    public void onBindViewHolder(@NonNull ProductViewHolder holder, int position) {
        try {
            ProductModel productModel = productModelList.get(position);
            holder.jName.setText(productModel.getName());
            holder.jCat.setText(productModel.getCategory_name());
            holder.jPrice.setText("\u20B9" + productModel.getPrice());

            if (!productModel.getImage().contentEquals("")) {
                Picasso.with(context)
                        .load(APIService.PRODUCT_BASE_URL + productModel.getImage())
                        .placeholder(R.drawable.progress_animation)
                        .error(R.drawable.ic_launcher_foreground)
                        .into(holder.jImage);
            }

            holder.productLay.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mListener.onClick(v, position);
                }
            });

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return productModelList.size();
    }

    public class ProductViewHolder extends RecyclerView.ViewHolder {

        LinearLayout productLay;
        TextView jName, jCat, jPrice;
        ImageView jImage;

        public ProductViewHolder(@NonNull View view) {
            super(view);
            productLay = view.findViewById(R.id.productLay);
            jName = view.findViewById(R.id.jName);
            jCat = view.findViewById(R.id.jCat);
            jPrice = view.findViewById(R.id.jPrice);
            jImage = view.findViewById(R.id.jImage);
        }
    }

}