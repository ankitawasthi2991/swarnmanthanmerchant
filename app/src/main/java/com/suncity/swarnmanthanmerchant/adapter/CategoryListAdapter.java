package com.suncity.swarnmanthanmerchant.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.suncity.swarnmanthanmerchant.R;
import com.suncity.swarnmanthanmerchant.helper.RecyclerViewClickListener;
import com.suncity.swarnmanthanmerchant.model.CategoryModel;

import java.util.ArrayList;

public class CategoryListAdapter extends RecyclerView.Adapter<CategoryListAdapter.ViewHolder> {

    private RecyclerViewClickListener mListener;

    private Context context;
    private ArrayList<CategoryModel> cityModelArrayList;

    public CategoryListAdapter(Context context, ArrayList<CategoryModel> addressModels, RecyclerViewClickListener listener) {
        this.context = context;
        this.cityModelArrayList = addressModels;
        mListener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
        View view = inflater.inflate(R.layout.category_list_item, viewGroup, false);
        return new ViewHolder(view);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, @SuppressLint("RecyclerView") int position) {

        viewHolder.tv_category.setText(cityModelArrayList.get(position).getName());
        viewHolder.tv_category.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onClick(v, position);
            }
        });

    }

    @Override
    public int getItemCount() {
        return cityModelArrayList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        TextView tv_category;

        ViewHolder(@NonNull View itemView) {
            super(itemView);
            tv_category = itemView.findViewById(R.id.tv_category);
        }

    }
}