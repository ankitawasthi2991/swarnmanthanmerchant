package com.suncity.swarnmanthanmerchant.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;
import com.suncity.swarnmanthanmerchant.R;
import com.suncity.swarnmanthanmerchant.helper.APIService;
import com.suncity.swarnmanthanmerchant.helper.RecyclerViewClickListener;
import com.suncity.swarnmanthanmerchant.model.EnquiryModel;

import java.util.ArrayList;
import java.util.List;

public class AdapterEnquiryListView extends RecyclerView.Adapter<AdapterEnquiryListView.EnquiryViewHolder> {
    private RecyclerViewClickListener mListener;
    private List<EnquiryModel> enquiryModelList;
    private Context context;

    public AdapterEnquiryListView(Context context, ArrayList<EnquiryModel> enquiryModelList, RecyclerViewClickListener listener) {
        this.context = context;
        this.enquiryModelList = enquiryModelList;
        mListener = listener;
    }

    @NonNull
    @Override
    public EnquiryViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View couponView =
                LayoutInflater.from(parent.getContext()).inflate(R.layout.cell_enquery, parent, false);
        return new EnquiryViewHolder(couponView);
    }

    @Override
    public void onBindViewHolder(@NonNull EnquiryViewHolder holder, int position) {
        try {
            EnquiryModel enquiryModel = enquiryModelList.get(position);
            holder.jName.setText(enquiryModel.getProduct_name());
            holder.jCat.setText(enquiryModel.getCategory_name());
            holder.jPrice.setText("\u20B9" + enquiryModel.getProduct_price());
            holder.jDesc.setText(enquiryModel.getDescription());

            holder.jUserName.setText(enquiryModel.getName());
            holder.jUserEmail.setText(enquiryModel.getEmail());
            holder.jUserNumber.setText(enquiryModel.getMobile());

            if (!enquiryModel.getProducts_image().contentEquals("")) {
                Picasso.with(context)
                        .load(APIService.PRODUCT_BASE_URL + enquiryModel.getProducts_image())
                        .placeholder(R.drawable.progress_animation)
                        .error(R.drawable.ic_launcher_foreground)
                        .into(holder.jImage);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return enquiryModelList.size();
    }

    public class EnquiryViewHolder extends RecyclerView.ViewHolder {

        LinearLayout productLay;
        TextView jName, jPrice, jCat, jDesc, jUserName, jUserNumber, jUserEmail;
        ImageView jImage;

        public EnquiryViewHolder(@NonNull View view) {
            super(view);
            productLay = view.findViewById(R.id.productLay);
            jName = view.findViewById(R.id.jName);
            jCat = view.findViewById(R.id.jCat);
            jPrice = view.findViewById(R.id.jPrice);
            jDesc = view.findViewById(R.id.jDesc);
            jUserName = view.findViewById(R.id.jUserName);
            jUserNumber = view.findViewById(R.id.jUserNumber);
            jUserEmail = view.findViewById(R.id.jUserEmail);
            jImage = view.findViewById(R.id.jImage);
        }
    }

}