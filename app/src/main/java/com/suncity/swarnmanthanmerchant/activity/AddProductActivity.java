package com.suncity.swarnmanthanmerchant.activity;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.FileProvider;
import androidx.loader.content.CursorLoader;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.suncity.swarnmanthanmerchant.BuildConfig;
import com.suncity.swarnmanthanmerchant.R;
import com.suncity.swarnmanthanmerchant.adapter.CategoryListAdapter;
import com.suncity.swarnmanthanmerchant.helper.APIService;
import com.suncity.swarnmanthanmerchant.helper.RecyclerViewClickListener;
import com.suncity.swarnmanthanmerchant.model.CategoryModel;
import com.suncity.swarnmanthanmerchant.util.FileOperation;
import com.suncity.swarnmanthanmerchant.util.SessionManager;
import com.suncity.swarnmanthanmerchant.util.VolleyMultipartRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class AddProductActivity extends AppCompatActivity {

    EditText et_product_name, et_product_price, et_description, et_material, et_product_weight,
            et_diamond_weight, et_size;
    TextView tv_category, tv_add_image;
    RelativeLayout rl_category;
    Button btn_submit;

    String strProductName, strCategory = "", strProductPrice, strDescription, strShopID, strToken,
            strMaterial, strProductWeight, strDiamondWeight, strSize;

    ImageView iv_product;
    Bitmap bitmap;
    Uri selectedImage;
    Boolean is_imgSelect = false;

    SessionManager sessionManager;

    ProgressDialog progressDialog;

    ArrayList<CategoryModel> categoryModelArrayList = new ArrayList<>();
    CategoryListAdapter categoryListAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_product);

        sessionManager = new SessionManager(AddProductActivity.this);
        strToken = sessionManager.getUserDetails().get(SessionManager.KEY_USER_TOKEN);
        strShopID = sessionManager.getUserDetails().get(SessionManager.KEY_USER_ID);

        et_product_name = findViewById(R.id.et_product_name);
        et_product_price = findViewById(R.id.et_product_price);
        et_description = findViewById(R.id.et_description);
        et_material = findViewById(R.id.et_material);
        et_product_weight = findViewById(R.id.et_product_weight);
        et_diamond_weight = findViewById(R.id.et_diamond_weight);
        et_size = findViewById(R.id.et_size);

        tv_category = findViewById(R.id.tv_category);
        tv_add_image = findViewById(R.id.tv_add_image);

        rl_category = findViewById(R.id.rl_category);

        iv_product = findViewById(R.id.iv_product);

        btn_submit = findViewById(R.id.btn_submit);

        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkValidation();
            }
        });

        tv_category.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                categoryDialog();
            }
        });

        iv_product.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectImage("productPic");
            }
        });

        getCategory();
    }

    private void checkValidation() {
        strProductName = et_product_name.getText().toString();
        strProductPrice = et_product_price.getText().toString();
        strDescription = et_description.getText().toString();
        strMaterial = et_material.getText().toString();
        strProductWeight = et_product_weight.getText().toString();
        strDiamondWeight = et_diamond_weight.getText().toString();
        strSize = et_size.getText().toString();
        if (strProductName.equals("")) {
            Toast.makeText(AddProductActivity.this, "Please Enter Product Name", Toast.LENGTH_LONG).show();
        } else if (strCategory.equals("")) {
            Toast.makeText(AddProductActivity.this, "Please Select Product Category", Toast.LENGTH_LONG).show();
        } else if (strProductPrice.equals("")) {
            Toast.makeText(AddProductActivity.this, "Please Enter Product Price", Toast.LENGTH_LONG).show();
        } else if (strDescription.equals("")) {
            Toast.makeText(AddProductActivity.this, "Please Enter Product Description", Toast.LENGTH_LONG).show();
        } else if (strMaterial.equals("")) {
            Toast.makeText(AddProductActivity.this, "Please Enter Material", Toast.LENGTH_LONG).show();
        } else if (strProductWeight.equals("")) {
            Toast.makeText(AddProductActivity.this, "Please Enter Product Weight", Toast.LENGTH_LONG).show();
        } else if (strDiamondWeight.equals("")) {
            Toast.makeText(AddProductActivity.this, "Please Enter Diamond Weight", Toast.LENGTH_LONG).show();
        } else if (!is_imgSelect) {
            Toast.makeText(AddProductActivity.this, "Please Select Product Image", Toast.LENGTH_LONG).show();
        } else if (strDescription.equals("")) {
            Toast.makeText(AddProductActivity.this, "Please Enter Size", Toast.LENGTH_LONG).show();
        } else {
            uploadProduct(selectedImage);
        }
    }

    private void selectImage(final String picValue) {
        final CharSequence[] options = {"Take Photo", "Select from Gallery", "Cancel"};
        AlertDialog.Builder builder = new AlertDialog.Builder(AddProductActivity.this);
        builder.setTitle("Add Product Photo!");
        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (options[item].equals("Take Photo")) {
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    File f = new File(Environment.getExternalStorageDirectory(), "temp.jpg");
                    Uri uri = FileProvider.getUriForFile(AddProductActivity.this,
                            BuildConfig.APPLICATION_ID + ".provider", f);
                    intent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
                    intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                    if (picValue.equalsIgnoreCase("productPic")) {
                        startActivityForResult(intent, 1);
                    }
                } else if (options[item].equals("Select from Gallery")) {
                    Intent intent = new Intent(Intent.ACTION_PICK,
                            MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    if (picValue.equalsIgnoreCase("productPic")) {
                        startActivityForResult(intent, 2);
                    }
                } else if (options[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == 1) {
                File f = new File(Environment.getExternalStorageDirectory().toString());
                for (File temp : f.listFiles()) {
                    if (temp.getName().equals("temp.jpg")) {
                        f = temp;
                        break;
                    }
                }
                try {
                    BitmapFactory.Options bitmapOptions = new BitmapFactory.Options();
                    bitmap = BitmapFactory.decodeFile(f.getAbsolutePath(),
                            bitmapOptions);
                    selectedImage = getImageUri(AddProductActivity.this, bitmap);
                    iv_product.setImageBitmap(bitmap);
                    is_imgSelect = true;
                    tv_add_image.setVisibility(View.GONE);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else if (requestCode == 2) {
                selectedImage = data.getData();
                String[] filePath = {MediaStore.Images.Media.DATA};
                Cursor c = getContentResolver().query(selectedImage, filePath, null, null, null);
                c.moveToFirst();
                int columnIndex = c.getColumnIndex(filePath[0]);
                String picturePath = c.getString(columnIndex);
                c.close();
                bitmap = (BitmapFactory.decodeFile(picturePath));
                iv_product.setImageBitmap(bitmap);
                is_imgSelect = true;
                tv_add_image.setVisibility(View.GONE);
            }
        }
    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    private void uploadProduct(Uri fileUri) {
        final ProgressDialog pDialog = new ProgressDialog(AddProductActivity.this);
        try {
            pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            pDialog.setMessage("Uploading...");
            pDialog.setIndeterminate(true);
            pDialog.setCanceledOnTouchOutside(false);
            pDialog.show();

            final File file = new File(getRealPathFromURI(fileUri));
            final FileOperation f = new FileOperation();
            f.readFile(file);

            String url;
            url = APIService.BASE_URL + "product";

            Log.d("Url", "" + url);
            VolleyMultipartRequest multipartRequest = new VolleyMultipartRequest(Request.Method.POST, url, new Response.Listener<NetworkResponse>() {
                @Override
                public void onResponse(NetworkResponse response) {
                    String resultResponse = new String(response.data);
                    Log.d("Response", resultResponse);
                    try {
                        JSONObject jsonObject = new JSONObject(resultResponse);
                        if (jsonObject.getString("status").equalsIgnoreCase("true")) {
                            pDialog.dismiss();
                            Toast.makeText(AddProductActivity.this, "Product Added Successfully", Toast.LENGTH_SHORT).show();
                            Intent mainIntent = new Intent(AddProductActivity.this, HomeActivity.class);
                            startActivity(mainIntent);
                            finish();
                        } else {
                            pDialog.dismiss();
                            Toast.makeText(AddProductActivity.this, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                        }
                    } catch (Exception ex) {
                        pDialog.dismiss();
                        ex.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    NetworkResponse networkResponse = error.networkResponse;
                    String errorMessage = "Unknown error";
                    if (networkResponse == null) {
                        if (error.getClass().equals(TimeoutError.class)) {
                            errorMessage = "Request timeout";
                        } else if (error.getClass().equals(NoConnectionError.class)) {
                            errorMessage = "Failed to connect server";
                        }
                    } else {
                        String result = new String(networkResponse.data);
                        try {
                            JSONObject response = new JSONObject(result);
                            String status = response.getString("status");
                            String message = response.getString("message");

                            Log.e("Error Status", status);
                            Log.e("Error Message", message);

                            if (networkResponse.statusCode == 404) {
                                errorMessage = "Resource not found";
                            } else if (networkResponse.statusCode == 401) {
                                errorMessage = message + " Please login again";
                            } else if (networkResponse.statusCode == 400) {
                                errorMessage = message + " Check your inputs";
                            } else if (networkResponse.statusCode == 500) {
                                errorMessage = message + " Something is getting wrong";
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    Log.i("Error", errorMessage);
                    pDialog.dismiss();
                    error.printStackTrace();
                }
            }) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<>();
                    params.put("name", strProductName);
                    params.put("category_id", strCategory);
                    params.put("price", strProductPrice);
                    params.put("description", strDescription);
                    params.put("material", strMaterial);
                    params.put("product_weight", strProductWeight);
                    params.put("diamond_weight", strDiamondWeight);
                    params.put("size", strSize);
                    params.put("shop_id", strShopID);
                    Log.d("params", params + "");
                    return params;
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("Accept", "application/json");
                    params.put("Authorization", "Bearer  " + strToken);
                    Log.d("params auth", params + "");
                    return params;
                }

                @Override
                protected Map<String, DataPart> getByteData() {
                    Map<String, DataPart> params = new HashMap<>();
                    try {
                        params.put("image", new DataPart("image.jpg", f.readFile(file), "image/jpeg"));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    return params;
                }
            };

            RequestQueue requestQueue = Volley.newRequestQueue(AddProductActivity.this);
            multipartRequest.setRetryPolicy(new DefaultRetryPolicy(500000, 0, 0));
            //Adding request to the queue
            requestQueue.add(multipartRequest);

        } catch (Exception ex) {
            Toast.makeText(AddProductActivity.this, ex.getMessage(), Toast.LENGTH_LONG).show();
            pDialog.dismiss();
        }

    }

    private String getRealPathFromURI(Uri contentUri) {
        String[] proj = {MediaStore.Images.Media.DATA};
        CursorLoader loader = new CursorLoader(AddProductActivity.this, contentUri, proj, null, null, null);
        Cursor cursor = loader.loadInBackground();
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        String result = cursor.getString(column_index);
        cursor.close();
        return result;
    }

    private void getCategory() {
        progressDialog = new ProgressDialog(AddProductActivity.this);
        progressDialog.setMessage("Get Categories....");
        progressDialog.show();

        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                okhttp3.Request original = chain.request();

                okhttp3.Request request = original.newBuilder()
                        .method(original.method(), original.body())
                        .build();

                return chain.proceed(request);
            }
        });

        OkHttpClient client = httpClient.build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(APIService.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();

        APIService api = retrofit.create(APIService.class);

        Call<JsonObject> call = api.getCategories();

        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, retrofit2.Response<JsonObject> response) {
                try {
                    JSONObject jsonObject = new JSONObject(new Gson().toJson(response.body()));
                    Log.d("resultCategories", jsonObject.toString());
                    if (jsonObject.getString("status").equalsIgnoreCase("true")) {
                        if (jsonObject.has("data")) {
                            JSONArray dataJsonArray = jsonObject.getJSONArray("data");
                            for (int i = 0; i < dataJsonArray.length(); i++) {
                                JSONObject dataObject = dataJsonArray.getJSONObject(i);
                                CategoryModel categoryModel = new CategoryModel();
                                categoryModel.setId(dataObject.getString("id"));
                                categoryModel.setName(dataObject.getString("name"));
                                categoryModelArrayList.add(categoryModel);
                            }
                            progressDialog.dismiss();
                        }
                    } else {
                        progressDialog.dismiss();
                        Toast.makeText(AddProductActivity.this, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                    progressDialog.dismiss();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                progressDialog.dismiss();
            }
        });

    }

    public void categoryDialog() {
        final Dialog dialog = new Dialog(AddProductActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_category);

        TextView rel_header_title = dialog.findViewById(R.id.rel_header_title);
        rel_header_title.setText("Select Category");

        RecyclerView recyclerview = dialog.findViewById(R.id.recyclerview);

        LinearLayoutManager layoutManager = new LinearLayoutManager(AddProductActivity.this, LinearLayoutManager.VERTICAL, false);
        recyclerview.setLayoutManager(layoutManager);
        categoryListAdapter = new CategoryListAdapter(AddProductActivity.this, categoryModelArrayList, new RecyclerViewClickListener() {
            @Override
            public void onClick(View view, int position) {
                strCategory = categoryModelArrayList.get(position).getId();
                tv_category.setText(categoryModelArrayList.get(position).getName());
                dialog.dismiss();
            }
        });
        recyclerview.setAdapter(categoryListAdapter);
        categoryListAdapter.notifyDataSetChanged();
        dialog.show();
    }

}