package com.suncity.swarnmanthanmerchant.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.suncity.swarnmanthanmerchant.R;

public class ContactUSActivity extends AppCompatActivity {

    ImageView iv_back;
    TextView tv_header, tv_about;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.about_us);

        iv_back = findViewById(R.id.iv_back);
        tv_header = findViewById(R.id.tv_header);
        tv_about = findViewById(R.id.tv_about);

        tv_header.setText("Contact US");

        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }
}