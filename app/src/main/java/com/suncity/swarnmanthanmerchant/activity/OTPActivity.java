package com.suncity.swarnmanthanmerchant.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.suncity.swarnmanthanmerchant.R;
import com.suncity.swarnmanthanmerchant.helper.APIService;
import com.suncity.swarnmanthanmerchant.util.SessionManager;

import org.json.JSONObject;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class OTPActivity extends AppCompatActivity {

    EditText et_otp;
    Button button_verify;
    TextView tv_resend;
    String strMobile, strOTP;
    String token, id, name, email, mobile, user_status, shop_id, shop_name, address;

    ProgressDialog progressdialog;
    Intent intent;

    SessionManager sessionManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otp);

        sessionManager = new SessionManager(OTPActivity.this);

        et_otp = findViewById(R.id.et_otp);
        button_verify = findViewById(R.id.button_verify);
        tv_resend = findViewById(R.id.tv_resend);
        tv_resend.setEnabled(false);

        intent = getIntent();
        if (intent != null) {
            strMobile = intent.getStringExtra("mobile_no");
            strOTP = intent.getStringExtra("otp");
            et_otp.setText(strOTP);
        }

        otpCounter();

        button_verify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                strOTP = et_otp.getText().toString().trim();
                if (strOTP.equals("")) {
                    Toast.makeText(OTPActivity.this, "Please Enter OTP.", Toast.LENGTH_LONG).show();
                } else {
                    getLogin();
                }
            }
        });

        tv_resend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendOTP();
            }
        });

    }

    public void otpCounter() {
        new CountDownTimer(180000, 1000) {

            public void onTick(long millisUntilFinished) {
                tv_resend.setText("You can resend OTP  after " + millisUntilFinished / 1000);
            }

            public void onFinish() {
                tv_resend.setEnabled(true);
                tv_resend.setText(R.string.resend_otp);
            }

        }.start();
    }

    private void getLogin() {
        progressdialog = new ProgressDialog(OTPActivity.this);
        progressdialog.setMessage("Get Details....");
        progressdialog.show();

        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request original = chain.request();

                Request request = original.newBuilder()
                        .method(original.method(), original.body())
                        .build();

                return chain.proceed(request);
            }
        });

        OkHttpClient client = httpClient.build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(APIService.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();

        APIService api = retrofit.create(APIService.class);

        Call<JsonObject> call = api.merchantLogin(strMobile, strOTP);

        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, retrofit2.Response<JsonObject> response) {
                try {
                    JSONObject jsonObject = new JSONObject(new Gson().toJson(response.body()));
                    Log.d("resultLogin", jsonObject.toString());
                    if (jsonObject.getString("status").equalsIgnoreCase("true")) {
                        progressdialog.dismiss();
                        Toast.makeText(OTPActivity.this, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                        if (jsonObject.has("token")) {
                            token = jsonObject.getString("token");
                            if (jsonObject.has("user_details")) {
                                JSONObject user_details = jsonObject.getJSONObject("user_details");
                                id = user_details.getString("id");
                                name = user_details.getString("name");
                                email = user_details.getString("email");
                                mobile = user_details.getString("mobile");
                                user_status = user_details.getString("status");
                            }
                            if (jsonObject.has("shop_details")) {
                                JSONObject shop_details = jsonObject.getJSONObject("shop_details");
                                shop_id = shop_details.getString("id");
                                shop_name = shop_details.getString("name");
                                address = shop_details.getString("address");
                            }
                            sessionManager.createLoginSession(token, id, name, mobile, email, user_status,
                                    shop_id, shop_name, address);
                        }
                        startActivity(new Intent(OTPActivity.this, HomeActivity.class));
                    } else {
                        progressdialog.dismiss();
                        Toast.makeText(OTPActivity.this, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                    progressdialog.dismiss();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                progressdialog.dismiss();
            }
        });

    }

    private void sendOTP() {
        progressdialog = new ProgressDialog(OTPActivity.this);
        progressdialog.setMessage("Sending OTP....");
        progressdialog.show();

        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request original = chain.request();

                Request request = original.newBuilder()
                        .method(original.method(), original.body())
                        .build();

                return chain.proceed(request);
            }
        });

        OkHttpClient client = httpClient.build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(APIService.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();

        APIService api = retrofit.create(APIService.class);

        Call<JsonObject> call = api.sendOTP(strMobile);

        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, retrofit2.Response<JsonObject> response) {
                try {
                    JSONObject jsonObject = new JSONObject(new Gson().toJson(response.body()));
                    Log.d("resultLogin", jsonObject.toString());
                    if (jsonObject.getString("status").equalsIgnoreCase("true")) {
                        progressdialog.dismiss();
                        Toast.makeText(OTPActivity.this, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                        otpCounter();
                    } else {
                        progressdialog.dismiss();
                        Toast.makeText(OTPActivity.this, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                    progressdialog.dismiss();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                progressdialog.dismiss();
            }
        });

    }

}