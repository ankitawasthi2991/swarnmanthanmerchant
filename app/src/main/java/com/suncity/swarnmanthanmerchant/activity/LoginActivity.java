package com.suncity.swarnmanthanmerchant.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.suncity.swarnmanthanmerchant.R;
import com.suncity.swarnmanthanmerchant.helper.APIService;

import org.json.JSONObject;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class LoginActivity extends AppCompatActivity {

    EditText et_mobile;
    Button button_sign_in;
    String strMobile;

    ProgressDialog progressdialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        et_mobile = findViewById(R.id.et_mobile);
        button_sign_in = findViewById(R.id.button_sign_in);

        button_sign_in.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                strMobile = et_mobile.getText().toString().trim();
                if (strMobile.equals("")) {
                    Toast.makeText(LoginActivity.this, "Please Enter Mobile Number.", Toast.LENGTH_LONG).show();
                } else {
                    sendOTP();
                }
            }
        });

    }

    private void sendOTP() {
        progressdialog = new ProgressDialog(LoginActivity.this);
        progressdialog.setMessage("Sending OTP....");
        progressdialog.show();

        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request original = chain.request();

                Request request = original.newBuilder()
                        .method(original.method(), original.body())
                        .build();

                return chain.proceed(request);
            }
        });

        OkHttpClient client = httpClient.build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(APIService.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();

        APIService api = retrofit.create(APIService.class);

        Call<JsonObject> call = api.sendOTP(strMobile);

        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, retrofit2.Response<JsonObject> response) {
                try {
                    JSONObject jsonObject = new JSONObject(new Gson().toJson(response.body()));
                    Log.d("resultLogin", jsonObject.toString());
                    if (jsonObject.getString("status").equalsIgnoreCase("true")) {
                        progressdialog.dismiss();
                        String strOTP = jsonObject.getString("otp_code");
                        startActivity(new Intent(LoginActivity.this, OTPActivity.class)
                                .putExtra("mobile_no", strMobile)
                                .putExtra("otp", strOTP));
                    } else {
                        progressdialog.dismiss();
                        Toast.makeText(LoginActivity.this, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                    progressdialog.dismiss();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                progressdialog.dismiss();
            }
        });

    }

}