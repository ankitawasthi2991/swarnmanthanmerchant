package com.suncity.swarnmanthanmerchant.helper;

import com.google.gson.JsonObject;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface APIService {
    //    String API_URL = "https://rudrakshatech.com/swarn-manthan/";
    String API_URL = "https://www.swarnmanthan.com/";
    String BASE_URL = API_URL + "api/";
    String IMAGE_BASE_URL = API_URL + "imgs/";
    String PRODUCT_BASE_URL = IMAGE_BASE_URL + "product/";

    @POST("login")
    @FormUrlEncoded
    Call<JsonObject> merchantLogin(@Field("mobile") String mobile,
                                   @Field("otp") String otp);

    @POST("send-otp")
    @FormUrlEncoded
    Call<JsonObject> sendOTP(
            @Field("mobile") String mobile);

    @POST("otp_verify")
    @FormUrlEncoded
    Call<JsonObject> verifyOTP(
            @Field("mobile") String mobile,
            @Field("otp") String otp);

    @GET("pages")
    @FormUrlEncoded
    Call<JsonObject> allPages();

    @GET("category")
    Call<JsonObject> getCategories();

    @GET("product")
    Call<JsonObject> getProduct();

    @Headers({"Content-Type: application/json;charset=UTF-8"})
    @GET("marchant/enquery")
    Call<JsonObject> getEnquiries(@Header("Authorization") String auth);

    @GET("page/about-us")
    Call<JsonObject> getAbout();

    @GET("page/term-and-condition")
    Call<JsonObject> getTerms();

    @POST("get_notifications")
    @FormUrlEncoded
    Call<JsonObject> PostNotification(
            @Field("store_id") String store_id);

    @POST("latitude_longitude")
    @FormUrlEncoded
    Call<JsonObject> updateLocation(
            @Field("latitude") String latitude,
            @Field("longitude") String longitude);

}