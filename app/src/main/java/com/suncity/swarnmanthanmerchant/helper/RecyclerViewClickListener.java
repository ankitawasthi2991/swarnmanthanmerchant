package com.suncity.swarnmanthanmerchant.helper;

import android.view.View;

public interface RecyclerViewClickListener {
    void onClick(View view, int position);
}