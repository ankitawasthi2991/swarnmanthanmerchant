package com.suncity.swarnmanthanmerchant.util;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import com.suncity.swarnmanthanmerchant.activity.LoginActivity;

import java.util.HashMap;

public class SessionManager {
    // Shared Preferences
    SharedPreferences pref;

    // Editor for Shared preferences
    Editor editor;

    // Context
    Context _context;

    // Shared pref mode
    int PRIVATE_MODE = 0;

    // SharedPref file name
    private static final String PREF_NAME = "SwarnManthanMerchant";

    // All Shared Preferences Keys
    private static final String IS_LOGIN = "IsLoggedIn";

    // User Token (make variable public to access from outside)
    public static final String KEY_USER_TOKEN = "user_token";

    // User Id (make variable public to access from outside)
    public static final String KEY_USER_ID = "user_id";

    // User name (make variable public to access from outside)
    public static final String KEY_USERNAME = "user_name";

    // Email address (make variable public to access from outside)
    public static final String KEY_EMAIL = "email";

    // Phone Number (make variable public to access from outside)
    public static final String KEY_PHONE = "mobile";

    // User GCM ID  (make variable public to access from outside)
    public static final String KEY_GCM_ID = "gcm_id";

    // User Device ID (make variable public to access from outside)
    public static final String KEY_DEVICE_ID = "device_id";

    // OTP Status  (make variable public to access from outside)
    public static final String KEY_USER_STATUS = "user_status";

    public static final String KEY_SHOP_ID = "shop_id";

    public static final String KEY_SHOP_NAME = "shop_name";

    public static final String KEY_SHOP_ADDRESS = "shop_address";

    // Constructor
    public SessionManager(Context context) {
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    /**
     * Create login session
     */
    public void createLoginSession(String token, String user_id, String name, String mobile, String email,
                                   String userStatus, String shop_id, String shop_name, String shop_address) {
        // Storing login value as TRUE
        editor.putBoolean(IS_LOGIN, true);
        editor.putString(KEY_USER_TOKEN, token);
        editor.putString(KEY_USER_ID, user_id);
        editor.putString(KEY_USERNAME, name);
        editor.putString(KEY_PHONE, mobile);
        editor.putString(KEY_EMAIL, email);
        editor.putString(KEY_USER_STATUS, userStatus);
        editor.putString(KEY_SHOP_ID, shop_id);
        editor.putString(KEY_SHOP_NAME, shop_name);
        editor.putString(KEY_SHOP_ADDRESS, shop_address);
        editor.commit();
    }

    /**
     * Create gcm session
     */
    public void createGcmId(String gcm_id) {
        // Storing login value as TRUE
        editor.putString(KEY_GCM_ID, gcm_id);
        editor.commit();
    }

    /**
     * Create gcm session
     */
    public void createDeviceId(String device_id) {
        // Storing login value as TRUE
        editor.putString(KEY_DEVICE_ID, device_id);
        editor.commit();
    }

    /**
     * Check login method wil check user login status
     * If false it will redirect user to login page
     * Else won't do anything
     */
    public void checkLogin() {
        // Check login status
        if (!this.isLoggedIn()) {

            // user is not logged in redirect him to Login Activity
            Intent i = new Intent(_context, LoginActivity.class);
            // Closing all the Activities
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

            // Add new Flag to start new Activity
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

            // Staring Login Activity
            _context.startActivity(i);
        }
    }

    /**
     * Get stored session data
     */
    public HashMap<String, String> getUserDetails() {
        HashMap<String, String> user = new HashMap<String, String>();

        user.put(KEY_USER_TOKEN, pref.getString(KEY_USER_TOKEN, ""));
        user.put(KEY_USER_ID, pref.getString(KEY_USER_ID, ""));
        user.put(KEY_USERNAME, pref.getString(KEY_USERNAME, ""));
        user.put(KEY_EMAIL, pref.getString(KEY_EMAIL, ""));
        user.put(KEY_PHONE, pref.getString(KEY_PHONE, ""));
        user.put(KEY_GCM_ID, pref.getString(KEY_GCM_ID, ""));
        user.put(KEY_USER_STATUS, pref.getString(KEY_USER_STATUS, ""));
        user.put(KEY_DEVICE_ID, pref.getString(KEY_DEVICE_ID, ""));
        user.put(KEY_SHOP_ID, pref.getString(KEY_SHOP_ID, ""));
        user.put(KEY_SHOP_NAME, pref.getString(KEY_SHOP_NAME, ""));
        user.put(KEY_SHOP_ADDRESS, pref.getString(KEY_SHOP_ADDRESS, ""));
        return user;
    }

    /**
     * Clear session details
     */
    public void logoutUser() {
        // Clearing all data from Shared Preferences
        editor.putBoolean(IS_LOGIN, false);
        pref.edit().remove(KEY_USER_TOKEN).commit();
        pref.edit().remove(KEY_USER_ID).commit();
        pref.edit().remove(KEY_USERNAME).commit();
        pref.edit().remove(KEY_EMAIL).commit();
        pref.edit().remove(KEY_PHONE).commit();
        pref.edit().remove(KEY_USER_STATUS).commit();
        pref.edit().remove(KEY_SHOP_ID).commit();
        pref.edit().remove(KEY_SHOP_NAME).commit();
        pref.edit().remove(KEY_SHOP_ADDRESS).commit();
        editor.commit();
    }

    public boolean getBoolean(String key) {
        return pref.getBoolean(key, false);
    }

    public void setBoolean(String key, boolean value) {
        editor = pref.edit();
        editor.putBoolean(key, value);
        editor.commit();
    }

    /**
     * Quick check for login
     **/
    // Get Login State
    public boolean isLoggedIn() {
        return pref.getBoolean(IS_LOGIN, false);
    }
}