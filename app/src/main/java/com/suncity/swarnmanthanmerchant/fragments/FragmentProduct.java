package com.suncity.swarnmanthanmerchant.fragments;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.suncity.swarnmanthanmerchant.R;
import com.suncity.swarnmanthanmerchant.activity.AddProductActivity;
import com.suncity.swarnmanthanmerchant.adapter.AdapterProductListView;
import com.suncity.swarnmanthanmerchant.helper.APIService;
import com.suncity.swarnmanthanmerchant.helper.RecyclerViewClickListener;
import com.suncity.swarnmanthanmerchant.model.ProductModel;
import com.suncity.swarnmanthanmerchant.util.SessionManager;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class FragmentProduct extends Fragment {

    RecyclerView rv_product;
    AdapterProductListView mAdapter;
    ArrayList<ProductModel> productModelArrayList = new ArrayList<>();
    FloatingActionButton fb_add_product;

    SessionManager sessionManager;
    String strToken;
    ProgressDialog progressDialog;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_product, container, false);

        sessionManager = new SessionManager(getActivity());
        strToken = sessionManager.getUserDetails().get(SessionManager.KEY_USER_TOKEN);

        fb_add_product = view.findViewById(R.id.fb_add_product);
        fb_add_product.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), AddProductActivity.class);
                startActivity(intent);
            }
        });

        rv_product = view.findViewById(R.id.rv_product);
        rv_product.setHasFixedSize(true);
        GridLayoutManager shopping_manager = new GridLayoutManager(getActivity(),
                2, GridLayoutManager.VERTICAL, false);
        rv_product.setLayoutManager(shopping_manager);

        getProducts();

        return view;
    }

    private void getProducts() {
        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("Get Products....");
        progressDialog.show();

        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request original = chain.request();

                Request request = original.newBuilder()
                        .addHeader("Authorization", "Bearer " + strToken)
                        .addHeader("Accept", "application/json")
                        .method(original.method(), original.body())
                        .build();

                return chain.proceed(request);
            }
        });

        OkHttpClient client = httpClient.build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(APIService.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();

        APIService api = retrofit.create(APIService.class);

        Call<JsonObject> call = api.getProduct();

        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, retrofit2.Response<JsonObject> response) {
                try {
                    JSONObject jsonObject = new JSONObject(new Gson().toJson(response.body()));
                    Log.d("resultProduct", jsonObject.toString());
                    if (jsonObject.getString("status").equalsIgnoreCase("true")) {
                        if (jsonObject.has("data")) {
                            JSONArray dataJsonArray = jsonObject.getJSONArray("data");
                            for (int i = 0; i < dataJsonArray.length(); i++) {
                                JSONObject dataObject = dataJsonArray.getJSONObject(i);
                                ProductModel productModel = new ProductModel();
                                productModel.setId(dataObject.getString("id"));
                                productModel.setName(dataObject.getString("name"));
                                productModel.setSlug(dataObject.getString("slug"));
                                productModel.setShop_id(dataObject.getString("shop_id"));
                                productModel.setCategory_id(dataObject.getString("category_id"));
                                productModel.setPrice(dataObject.getString("price"));
                                productModel.setDescription(dataObject.getString("description"));
                                productModel.setCategory_name(dataObject.getString("category_name"));
                                if (dataObject.has("image")) {
                                    productModel.setImage(dataObject.getString("image"));
                                }
                                productModel.setStatus(dataObject.getString("status"));
                                if (dataObject.has("productimage")) {
                                    JSONArray productImageJsonArray = dataObject.getJSONArray("productimage");
                                    if (productImageJsonArray.length() > 0) {
                                        productModel.setProductimage(productImageJsonArray.toString());
                                    } else {
                                        productModel.setProductimage("");
                                    }
                                }
                                productModelArrayList.add(productModel);
                            }
                            progressDialog.dismiss();
                            if (productModelArrayList.size() > 0) {
                                rv_product.setVisibility(View.VISIBLE);
                                mAdapter = new AdapterProductListView(getActivity(), productModelArrayList, new RecyclerViewClickListener() {
                                    @Override
                                    public void onClick(View view, int position) {

                                    }
                                });
                                rv_product.setAdapter(mAdapter);
                            } else {
                                rv_product.setVisibility(View.GONE);
                            }
                        }
                    } else {
                        progressDialog.dismiss();
                        Toast.makeText(getActivity(), jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                    progressDialog.dismiss();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                progressDialog.dismiss();
            }
        });

    }

}