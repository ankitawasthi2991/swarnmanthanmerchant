package com.suncity.swarnmanthanmerchant.fragments;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.suncity.swarnmanthanmerchant.R;
import com.suncity.swarnmanthanmerchant.activity.AboutActivity;
import com.suncity.swarnmanthanmerchant.activity.LoginActivity;
import com.suncity.swarnmanthanmerchant.activity.TermsActivity;
import com.suncity.swarnmanthanmerchant.helper.APIService;
import com.suncity.swarnmanthanmerchant.util.GpsTracker;
import com.suncity.swarnmanthanmerchant.util.SessionManager;

import org.json.JSONObject;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class FragmentProfile extends Fragment {

    TextView tv_shop_name, tv_mobile_no, tv_email, tv_address, tv_subscription, tv_about, tv_contact, tv_terms,
            tv_logout;
    ImageView iv_location;

    SessionManager sessionManager;
    String strToken, strLatitude, strLongitude;
    ProgressDialog progressDialog;

    GpsTracker gpsTracker;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_profile, container, false);

        sessionManager = new SessionManager(getActivity());
        strToken = sessionManager.getUserDetails().get(SessionManager.KEY_USER_TOKEN);

        try {
            if (ContextCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(getActivity(), new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION}, 101);
            } else {
                getLocation();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        tv_shop_name = view.findViewById(R.id.tv_shop_name);
        tv_mobile_no = view.findViewById(R.id.tv_mobile_no);
        tv_email = view.findViewById(R.id.tv_email);
        tv_address = view.findViewById(R.id.tv_address);
        tv_subscription = view.findViewById(R.id.tv_subscription);
        tv_about = view.findViewById(R.id.tv_about);
        tv_contact = view.findViewById(R.id.tv_contact);
        tv_terms = view.findViewById(R.id.tv_terms);
        tv_logout = view.findViewById(R.id.tv_logout);

        iv_location = view.findViewById(R.id.iv_location);

        tv_shop_name.setText(sessionManager.getUserDetails().get(SessionManager.KEY_USERNAME));
        tv_mobile_no.setText(sessionManager.getUserDetails().get(SessionManager.KEY_PHONE));
        tv_email.setText(sessionManager.getUserDetails().get(SessionManager.KEY_EMAIL));
        tv_address.setText(sessionManager.getUserDetails().get(SessionManager.KEY_SHOP_ADDRESS));

        tv_subscription.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        tv_about.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), AboutActivity.class);
                startActivity(intent);
            }
        });

        tv_contact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        tv_terms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), TermsActivity.class);
                startActivity(intent);
            }
        });

        iv_location.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateLocation();
            }
        });

        tv_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sessionManager.logoutUser();
                Intent intent = new Intent(getActivity(), LoginActivity.class);
                startActivity(intent);
                getActivity().finish();
            }
        });
        return view;
    }

    public void getLocation() {
        gpsTracker = new GpsTracker(getActivity());
        if (gpsTracker.canGetLocation()) {
            double latitude = gpsTracker.getLatitude();
            double longitude = gpsTracker.getLongitude();
            strLatitude = String.valueOf(latitude);
            strLongitude = String.valueOf(longitude);
        } else {
            gpsTracker.showSettingsAlert();
        }
    }

    private void updateLocation() {
        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("Updating Location....");
        progressDialog.show();

        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request original = chain.request();

                Request request = original.newBuilder()
                        .addHeader("Authorization", "Bearer " + strToken)
                        .addHeader("Accept", "application/json")
                        .method(original.method(), original.body())
                        .build();

                return chain.proceed(request);
            }
        });

        OkHttpClient client = httpClient.build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(APIService.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();

        APIService api = retrofit.create(APIService.class);

        Call<JsonObject> call = api.updateLocation(strLatitude, strLongitude);

        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, retrofit2.Response<JsonObject> response) {
                try {
                    JSONObject jsonObject = new JSONObject(new Gson().toJson(response.body()));
                    Log.d("resultLogin", jsonObject.toString());
                    if (jsonObject.getString("status").equalsIgnoreCase("success")) {
                        progressDialog.dismiss();
                        Toast.makeText(getActivity(), "Your Shop Location is updated successfully.", Toast.LENGTH_SHORT).show();
                    } else {
                        progressDialog.dismiss();
                        Toast.makeText(getActivity(), jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                    progressDialog.dismiss();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                progressDialog.dismiss();
            }
        });

    }

}