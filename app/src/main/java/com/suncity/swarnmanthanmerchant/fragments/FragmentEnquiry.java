package com.suncity.swarnmanthanmerchant.fragments;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.suncity.swarnmanthanmerchant.R;
import com.suncity.swarnmanthanmerchant.adapter.AdapterEnquiryListView;
import com.suncity.swarnmanthanmerchant.helper.APIService;
import com.suncity.swarnmanthanmerchant.helper.RecyclerViewClickListener;
import com.suncity.swarnmanthanmerchant.model.EnquiryModel;
import com.suncity.swarnmanthanmerchant.util.SessionManager;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class FragmentEnquiry extends Fragment {

    RecyclerView rv_enquiry;
    AdapterEnquiryListView mAdapter;
    ArrayList<EnquiryModel> enquiryModelArrayList = new ArrayList<>();

    SessionManager sessionManager;
    String strToken;
    ProgressDialog progressDialog;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_enquiry, container, false);

        sessionManager = new SessionManager(getActivity());
        strToken = sessionManager.getUserDetails().get(SessionManager.KEY_USER_TOKEN);

        rv_enquiry = view.findViewById(R.id.rv_enquiry);
        rv_enquiry.setHasFixedSize(true);
        GridLayoutManager shopping_manager = new GridLayoutManager(getActivity(),
                1, GridLayoutManager.VERTICAL, false);
        rv_enquiry.setLayoutManager(shopping_manager);

        getEnquiries();

        return view;
    }

    private void getEnquiries() {
        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("Get Enquiries....");
        progressDialog.show();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(APIService.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        APIService api = retrofit.create(APIService.class);

        Call<JsonObject> call = api.getEnquiries("Bearer " + strToken);

        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, retrofit2.Response<JsonObject> response) {
                try {
                    JSONObject jsonObject = new JSONObject(new Gson().toJson(response.body()));
                    Log.d("resultEnquiry", jsonObject.toString());
                    if (jsonObject.getString("status").equalsIgnoreCase("true")) {
                        if (jsonObject.has("data")) {
                            JSONArray dataJsonArray = jsonObject.getJSONArray("data");
                            for (int i = 0; i < dataJsonArray.length(); i++) {
                                JSONObject dataObject = dataJsonArray.getJSONObject(i);
                                EnquiryModel enquiryModel = new EnquiryModel();
                                enquiryModel.setId(dataObject.getString("id"));
                                enquiryModel.setProduct_id(dataObject.getString("product_id"));
                                enquiryModel.setShop_id(dataObject.getString("shop_id"));
                                enquiryModel.setUser_id(dataObject.getString("user_id"));
                                enquiryModel.setName(dataObject.getString("name"));
                                enquiryModel.setMobile(dataObject.getString("mobile"));
                                enquiryModel.setEmail(dataObject.getString("email"));
                                enquiryModel.setDescription(dataObject.getString("description"));
                                enquiryModel.setProduct_name(dataObject.getString("product_name"));
                                enquiryModel.setProduct_price(dataObject.getString("product_price"));
                                enquiryModel.setCategory_name(dataObject.getString("category_name"));
                                enquiryModel.setProducts_image(dataObject.getString("products_image"));
                                enquiryModelArrayList.add(enquiryModel);
                            }
                            progressDialog.dismiss();
                            if (enquiryModelArrayList.size() > 0) {
                                rv_enquiry.setVisibility(View.VISIBLE);
                                mAdapter = new AdapterEnquiryListView(getActivity(), enquiryModelArrayList, new RecyclerViewClickListener() {
                                    @Override
                                    public void onClick(View view, int position) {

                                    }
                                });
                                rv_enquiry.setAdapter(mAdapter);
                            } else {
                                rv_enquiry.setVisibility(View.GONE);
                            }
                        }
                    } else {
                        progressDialog.dismiss();
                        Toast.makeText(getActivity(), jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                    progressDialog.dismiss();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                progressDialog.dismiss();
            }
        });

    }

}